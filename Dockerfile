ARG SOURCE_DOCKER_REGISTRY=localhost:5000

FROM ${SOURCE_DOCKER_REGISTRY}/alpine_opt_bcftools:1.10.2 as opt_bcftools
FROM ${SOURCE_DOCKER_REGISTRY}/alpine_opt_samtools:1.10 as opt_samtools

FROM ${SOURCE_DOCKER_REGISTRY}/alpine_tools:3.11.3 as build

RUN apk update && \
    apk add zlib-dev python2 boost-dev boost-static

RUN mkdir -p /tmp/scratch/build && \
    cd /tmp/scratch/ && wget -t 1 http://localhost:8000/strelka-2.9.10.release_src.tar.bz2 || \
                        wget -t 3 https://github.com/Illumina/strelka/releases/download/v2.9.10/strelka-2.9.10.release_src.tar.bz2 && \
    cd /tmp/scratch/ && tar -xvf strelka-2.9.10.release_src.tar.bz2 && \
    cd /tmp/scratch/build/ && cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/opt ../strelka-2.9.10.release_src/ && make -j$(nproc) && make install && \
    cd / && rm -rf /tmp/scratch

FROM ${SOURCE_DOCKER_REGISTRY}/alpine_base:3.11.3

COPY --from=opt_bcftools /opt /opt
COPY --from=opt_samtools /opt /opt
COPY --from=build /opt /opt

ENV PATH /opt/bin/:/usr/bin/:/bin/:/usr/sbin/:/sbin/
ENV LD_LIBRARY_PATH /opt/lib/

RUN mkdir -p /opt/bin/ && echo "#!/bin/bash" > /opt/bin/module && chmod a+x /opt/bin/module

RUN apk update && \
    apk add python2
